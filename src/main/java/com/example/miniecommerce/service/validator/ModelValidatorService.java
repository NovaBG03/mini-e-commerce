package com.example.miniecommerce.service.validator;

import com.example.miniecommerce.exception.EcomHttpException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ModelValidatorService {
    private final Validator validator;
    private final ValidatorProps validatorProps;

    public void validatePassword(String password) {
        final var passwordMinLength = validatorProps.getPasswordMinLength();
        if (password == null || password.length() < passwordMinLength) {
            throw new EcomHttpException("USER_PASSWORD_MIN_LENGTH_" + passwordMinLength, HttpStatus.BAD_REQUEST);
        }

        final var passwordMaxLength = validatorProps.getPasswordMaxLength();
        if (password.length() > passwordMaxLength) {
            throw new EcomHttpException("USER_PASSWORD_MAX_LENGTH_" + passwordMaxLength, HttpStatus.BAD_REQUEST);
        }
    }

    public <T> void validate(T obj) {
        List<String> errorMessages = getErrorMessages(obj);

        if (errorMessages.size() > 0) {
            errorMessages.sort(String::compareTo);

            errorMessages.forEach(errorMessage -> {
                throw new EcomHttpException(errorMessage, HttpStatus.BAD_REQUEST);
            });
        }
    }

    private <T> List<String> getErrorMessages(T obj) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(obj);

        return constraintViolations
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
    }
}
