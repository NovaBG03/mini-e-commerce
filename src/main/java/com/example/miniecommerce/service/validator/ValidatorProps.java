package com.example.miniecommerce.service.validator;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("ecom.validation")
public class ValidatorProps {
    private int passwordMinLength;
    private int passwordMaxLength;
}
