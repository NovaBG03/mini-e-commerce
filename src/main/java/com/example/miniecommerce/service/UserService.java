package com.example.miniecommerce.service;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.purchase.Cart;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.repository.UserRepository;
import com.example.miniecommerce.security.EcomUserDetails;
import com.example.miniecommerce.service.validator.ModelValidatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelValidatorService modelValidatorService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new EcomUserDetails(getUserByUsername(username));
    }

    public EcomUser getUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @Transactional
    public EcomUser register(EcomUser user, String password) {
        user.setId(null);

        if (userRepository.existsByUsername(user.getUsername())) {
            throw new EcomHttpException("USER_USERNAME_EXISTS", HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(user.getEmail())) {
            throw new EcomHttpException("USER_EMAIL_EXISTS", HttpStatus.BAD_REQUEST);
        }

        modelValidatorService.validatePassword(password);
        user.setEncodedPassword(passwordEncoder.encode(password));

        user.setCart(Cart.builder().user(user).build());

        modelValidatorService.validate(user);
        return userRepository.save(user);
    }

    @Transactional
    public EcomUser updateUser(EcomUser newUserData, EcomUser user) {
        final var newUsername = newUserData.getUsername();
        if (newUsername != null && !newUsername.equals(user.getUsername())) {
            if (userRepository.existsByUsername(newUsername)) {
                throw new EcomHttpException("USER_USERNAME_EXISTS", HttpStatus.BAD_REQUEST);
            }
            user.setUsername(newUsername);
        }

        if (newUserData.getFirstName() != null) {
            user.setFirstName(newUserData.getFirstName());
        }

        if (newUserData.getLastName() != null) {
            user.setLastName(newUserData.getLastName());
        }

        final var newEmail = newUserData.getEmail();
        if (newEmail != null && !newEmail.equals(user.getEmail())) {
            if (userRepository.existsByEmail(newEmail)) {
                throw new EcomHttpException("USER_EMAIL_EXISTS", HttpStatus.BAD_REQUEST);
            }
            user.setEmail(newEmail);
        }

        if (newUserData.getPhoneNumber() != null) {
            user.setPhoneNumber(newUserData.getPhoneNumber());
        }

        if (newUserData.getAddress() != null) {
            user.setAddress(newUserData.getAddress());
        }

        if (newUserData.getDeliveryPreferences() != null) {
            user.setDeliveryPreferences(newUserData.getDeliveryPreferences());
        }

        modelValidatorService.validate(user);
        return userRepository.save(user);
    }
}
