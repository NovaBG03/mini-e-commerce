package com.example.miniecommerce.service.jwt;

import com.example.miniecommerce.exception.EcomHttpException;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JwtService {
    private final UserDetailsService userDetailsService;
    private final JwtProps jwtProps;

    public Authentication getAuthentication(HttpServletRequest request) {
        String authorizationToken = request.getHeader(jwtProps.getHttpHeader());
        return this.getAuthentication(authorizationToken);
    }

    public Authentication getAuthentication(String authorizationToken) {
        if (authorizationToken == null
                || authorizationToken.isBlank()
                || !authorizationToken.startsWith(jwtProps.getTokenPrefix())) {
            return null;
        }

        String token = authorizationToken.replace(jwtProps.getTokenPrefix(), "").strip();
        if (token == null || token.isBlank()) {
            return null;
        }

        Jws<Claims> claimsJws;
        try {
            claimsJws = Jwts.parserBuilder()
                    .setSigningKey(jwtProps.getSecretKey())
                    .build()
                    .parseClaimsJws(token);
        } catch (ExpiredJwtException e) {
            throw new EcomHttpException("JWT_EXPIRED", HttpStatus.UNAUTHORIZED);
        } catch (JwtException e) {
            throw new EcomHttpException("JWT_INVALID", HttpStatus.UNAUTHORIZED);
        }

        Claims body = claimsJws.getBody();

        String username = body.getSubject();

        var authorities = (List<Map<String, String>>) body.get("authorities");

        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = authorities.stream()
                .map(authorityMap -> new SimpleGrantedAuthority(authorityMap.get("authority")))
                .collect(Collectors.toSet());

        return new UsernamePasswordAuthenticationToken(
                username,
                null,
                simpleGrantedAuthorities
        );
    }

    public void setAuthorizationResponseHeaderForUser(HttpServletResponse response, String username) {
        final String jwt = this.createJwt(username);
        this.setAuthorizationResponseHeader(response, jwt);
    }

    public String createJwt(String username) {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .claim("authorities", userDetails.getAuthorities())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plusSeconds(jwtProps.getTokenExpirationSeconds())))
                .signWith(jwtProps.getSecretKey())
                .compact();
    }

    private void setAuthorizationResponseHeader(HttpServletResponse response, String jwt) {
        response.addHeader("Access-Control-Expose-Headers", jwtProps.getHttpHeader());
        response.setHeader(jwtProps.getHttpHeader(), jwtProps.getTokenPrefix() + " " + jwt);
    }
}
