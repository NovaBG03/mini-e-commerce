package com.example.miniecommerce.service.jwt;

import io.jsonwebtoken.security.Keys;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpHeaders;

import java.security.Key;


@Getter
@Setter
@ConfigurationProperties("ecom.jwt")
public class JwtProps {
    private String secretKeyString;
    private String tokenPrefix;
    private Long tokenExpirationSeconds;
    private String httpHeader = HttpHeaders.AUTHORIZATION;

    public Key getSecretKey() {
        return Keys.hmacShaKeyFor(secretKeyString.getBytes());
    }
}
