package com.example.miniecommerce.service;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.repository.ProductRepository;
import com.example.miniecommerce.service.validator.ModelValidatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ModelValidatorService validatorService;

    public Product createProduct(Product product) {
        product.setId(null);
        return saveOrUpdateProduct(product);
    }

    public Product saveOrUpdateProduct(Product product) {
        if (productRepository.existsByCodeAndIdIsNot(product.getCode(), product.getId())) {
            throw new EcomHttpException("PRODUCT_CODE_EXISTS", HttpStatus.BAD_REQUEST);
        }

        validatorService.validate(product);
        return productRepository.save(product);
    }

    public Product getById(Long id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new EcomHttpException("PRODUCT_NOT_FOUND", HttpStatus.NOT_FOUND));
    }

    public void deleteById(Long id) {
        if (productRepository.existsById(id)) {
            productRepository.deleteById(id);
            return;
        }

        throw new EcomHttpException("PRODUCT_NOT_FOUND", HttpStatus.NOT_FOUND);
    }

    public Page<Product> getFilteredProducts(Specification<Product> specification, Pageable pageable) {
        return productRepository.findAll(specification, pageable);
    }
}
