package com.example.miniecommerce.service;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.purchase.Order;
import com.example.miniecommerce.repository.OrderRepository;
import com.example.miniecommerce.repository.ProductRepository;
import com.example.miniecommerce.service.validator.ModelValidatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final ModelValidatorService validatorService;
    private final CartService cartService;

    @Transactional
    public Order placeOrder(EcomUser user) {
        final var cart = user.getCart();

        if (cart.isEmpty()) {
            throw new EcomHttpException("CART_EMPTY", HttpStatus.BAD_REQUEST);
        }

        final var order = Order.from(cart);
        user.addOrder(order);

        final var updatedProducts = order.getRequestedProducts().stream()
                .map(requestedProduct -> {
                    final var selectedQuantity = requestedProduct.getSelectedQuantity();
                    final var product = requestedProduct.getProduct();
                    product.removeQuantity(selectedQuantity);

                    if (product.getQuantity() < 0) {
                        throw new EcomHttpException("INVALID_QUANTITY_IN_CART_FOR_PRODUCT_" + product.getId(), HttpStatus.BAD_REQUEST);
                    }
                    return product;
                })
                .collect(Collectors.toList());
        productRepository.saveAll(updatedProducts);

        cartService.emptyCartFor(user);

        validatorService.validate(order);
        return orderRepository.save(order);
    }
}
