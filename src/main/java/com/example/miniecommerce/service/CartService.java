package com.example.miniecommerce.service;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.purchase.Cart;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.repository.RequestedProductRepository;
import com.example.miniecommerce.repository.CartRepository;
import com.example.miniecommerce.service.validator.ModelValidatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;

@Service
@RequiredArgsConstructor
public class CartService {
    private final ProductService productService;
    private final ModelValidatorService validatorService;
    private final CartRepository cartRepository;
    private final RequestedProductRepository requestedProductRepository;


    @Transactional
    public Cart addProduct(Long productId, int productQuantity, EcomUser user) {
        if (productQuantity <= 0) {
            throw new EcomHttpException("PRODUCT_QUANTITY_INVALID", HttpStatus.BAD_REQUEST);
        }

        final var cart = user.getCart();
        final var product = productService.getById(productId);

        final var requestedProduct = cart.addProduct(product, productQuantity);

        if (requestedProduct.getSelectedQuantity() > product.getQuantity()) {
            throw new EcomHttpException("PRODUCT_QUANTITY_EXCEEDS_AVAILABLE", HttpStatus.BAD_REQUEST);
        }

        validatorService.validate(requestedProduct);
        validatorService.validate(cart);
        return cartRepository.save(cart);
    }

    @Transactional
    public Cart removeProduct(Long productId, int productQuantity, EcomUser user) {
        if (productQuantity <= 0) {
            throw new EcomHttpException("PRODUCT_QUANTITY_INVALID", HttpStatus.BAD_REQUEST);
        }

        final Cart cart = user.getCart();
        final var product = productService.getById(productId);

        var requestedProduct = cart.removeProduct(product, productQuantity);

        if (requestedProduct == null) {
            throw new EcomHttpException("PRODUCT_NOT_IN_CART", HttpStatus.NOT_FOUND);
        }

        if (requestedProduct.getSelectedQuantity() <= 0) {
            requestedProductRepository.delete(requestedProduct);
        } else {
            validatorService.validate(requestedProduct);
        }

        validatorService.validate(cart);
        return cartRepository.save(cart);
    }

    @Transactional
    public Cart emptyCartFor(EcomUser user) {
        final var cart = user.getCart();
        requestedProductRepository.deleteAll(cart.getRequestedProducts());
        cart.setRequestedProducts(new HashSet<>());
        return cart;
    }
}
