package com.example.miniecommerce.bootstrap;

import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.model.product.ProductStatus;
import com.example.miniecommerce.repository.ProductRepository;
import com.example.miniecommerce.repository.UserRepository;
import com.example.miniecommerce.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
@Profile("dev")
@RequiredArgsConstructor
public class Bootstrap implements CommandLineRunner {
    private final UserRepository userRepository;
    private final UserService userService;
    private final ProductRepository productRepository;

    @Override
    public void run(String... args) {
        if (userRepository.count() == 0) {
            loadUsers();
        }
        if (productRepository.count() == 0) {
            loadProducts();
        }
    }

    private void loadUsers() {
        userService.register(
                EcomUser.builder()
                        .username("ivan")
                        .firstName("Ivan")
                        .lastName("Ivanov")
                        .email("ivan@abv.bg")
                        .phoneNumber("1234567890")
                        .address("address 123 city")
                        .deliveryPreferences("idk bro")
                        .build(),
                "ivan123"
        );
    }

    private void loadProducts() {
        productRepository.saveAll(List.of(
                Product.builder()
                        .code("SM2132")
                        .name("Samsung Galaxy S22")
                        .description("The best phone for everyday use.")
                        .price(BigDecimal.valueOf(2000))
                        .quantity(4)
                        .status(ProductStatus.ONLINE)
                        .build(),
                Product.builder()
                        .code("SM2122")
                        .name("Samsung Galaxy S22 Ultra")
                        .description("The best phone on the market.")
                        .price(BigDecimal.valueOf(2400))
                        .quantity(300)
                        .status(ProductStatus.ONLINE)
                        .build(),
                Product.builder()
                        .code("SM2133")
                        .name("Samsung Galaxy A71")
                        .description("Good phone, good money")
                        .price(BigDecimal.valueOf(10))
                        .quantity(430)
                        .status(ProductStatus.ONLINE)
                        .build(),
                Product.builder()
                        .code("SM2134")
                        .name("Samsung Galaxy A52")
                        .description("The perfect gift for your child...")
                        .price(BigDecimal.valueOf(10))
                        .quantity(10)
                        .status(ProductStatus.ONLINE)
                        .build(),
                Product.builder()
                        .code("IP2135")
                        .name("Iphone 13")
                        .description("You need this phone now!")
                        .price(BigDecimal.valueOf(2100))
                        .quantity(140)
                        .status(ProductStatus.ONLINE)
                        .build(),
                Product.builder()
                        .code("IP2300")
                        .name("Iphone 13 Pro")
                        .description("Are you sure you need this?")
                        .price(BigDecimal.valueOf(3000))
                        .quantity(200)
                        .status(ProductStatus.ONLINE)
                        .build()
        ));
    }
}
