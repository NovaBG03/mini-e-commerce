package com.example.miniecommerce.exception;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @Bean
    public ErrorAttributes errorAttributes() {
        // Hide exception fields in the returned object
        return new DefaultErrorAttributes() {
            @Override
            public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, options);
                errorAttributes.remove("exception");
                errorAttributes.remove("trace");
                return errorAttributes;
            }
        };
    }

    @ExceptionHandler(EcomHttpException.class)
    public void handleCustomException(HttpServletResponse res, EcomHttpException e) throws IOException {
        if (!res.isCommitted()) {
            res.sendError(e.getHttpStatus().value(), e.getMessage());
        }
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public void handleUsernameNotFoundException(HttpServletResponse res) throws IOException {
        if (!res.isCommitted()) {
            res.sendError(HttpStatus.NOT_FOUND.value(), "USER_NOT_FOUND");
        }
    }

    @ExceptionHandler(AccessDeniedException.class)
    public void handleAccessDeniedException(HttpServletResponse res) throws IOException {
        if (!res.isCommitted()) {
            res.sendError(HttpStatus.FORBIDDEN.value(), "ACCESS_DENIED");
        }
    }

    @ExceptionHandler(Exception.class)
    public void handleException(HttpServletResponse res) throws IOException {
        if (!res.isCommitted()) {
            res.sendError(HttpStatus.BAD_REQUEST.value(), "SOMETHING_WENT_WRONG");
        }
    }
}
