package com.example.miniecommerce.controller;

import com.example.miniecommerce.dto.user.request.UserPatchRequestDto;
import com.example.miniecommerce.dto.user.request.UserRegisterRequestDto;
import com.example.miniecommerce.dto.user.response.UserDataResponseDto;
import com.example.miniecommerce.mapper.user.UserMapper;
import com.example.miniecommerce.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    @GetMapping
    public UserDataResponseDto getPrincipalData(Principal principal) {
        return userMapper.userToUserDataResponseDto(userService.getUserByUsername(principal.getName()));
    }

    @PostMapping("/register")
    public UserDataResponseDto register(@RequestBody UserRegisterRequestDto dto) {
        return userMapper.userToUserDataResponseDto(
                userService.register(userMapper.userRegisterRequestDtoToUser(dto), dto.getPassword())
        );
    }

    @PatchMapping
    public UserDataResponseDto patchPrincipalData(@RequestBody UserPatchRequestDto dto, Principal principal) {
        return userMapper.userToUserDataResponseDto(
                userService.updateUser(
                        userMapper.userPatchRequestDtoToUser(dto),
                        userService.getUserByUsername(principal.getName())
                )
        );
    }
}
