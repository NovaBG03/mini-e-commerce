package com.example.miniecommerce.controller;

import com.example.miniecommerce.dto.order.response.OrderResponseDto;
import com.example.miniecommerce.mapper.order.OrderMapper;
import com.example.miniecommerce.service.OrderService;
import com.example.miniecommerce.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("api/v1/orders")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final UserService userService;
    private final OrderMapper orderMapper;

    @PostMapping
    public OrderResponseDto placeOrder(Principal principal) {
        return orderMapper.orderToOrderResponseDto(
                orderService.placeOrder(userService.getUserByUsername(principal.getName()))
        );
    }
}
