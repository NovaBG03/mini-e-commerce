package com.example.miniecommerce.controller;

import com.example.miniecommerce.dto.PageableRequest;
import com.example.miniecommerce.dto.product.request.ProductCreateRequestDto;
import com.example.miniecommerce.dto.product.request.ProductUpdateRequestDto;
import com.example.miniecommerce.dto.product.response.ProductPageResponseDto;
import com.example.miniecommerce.dto.product.response.ProductResponseDto;
import com.example.miniecommerce.filtering.product.ProductFilterCriteria;
import com.example.miniecommerce.filtering.product.ProductSpecificationFactory;
import com.example.miniecommerce.mapper.product.ProductMapper;
import com.example.miniecommerce.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final ProductMapper productMapper;
    private final ProductSpecificationFactory productSpecificationFactory;

    @GetMapping("{id}")
    public ProductResponseDto getProduct(@PathVariable Long id) {
        return productMapper.productToProductResponseDto(
                productService.getById(id)
        );
    }

    @GetMapping
    public ProductPageResponseDto getProducts(PageableRequest pageableRequest,
                                              ProductFilterCriteria productFilterCriteria) {
        return productMapper.productPageToProductPageResponseDto(
                productService.getFilteredProducts(
                        productSpecificationFactory.createSpecification(productFilterCriteria),
                        pageableRequest.get()
                )
        );
    }

    @PostMapping
    public ProductResponseDto createProduct(@RequestBody ProductCreateRequestDto dto) {
        return productMapper.productToProductResponseDto(
                productService.createProduct(productMapper.productCreateRequestDtoToProduct(dto))
        );
    }

    @PutMapping
    public ProductResponseDto saveOrUpdateProduct(@RequestBody ProductUpdateRequestDto dto) {
        return productMapper.productToProductResponseDto(
                productService.saveOrUpdateProduct(productMapper.productUpdateRequestDtoToProduct(dto))
        );
    }

    @DeleteMapping("{id}")
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteById(id);
    }
}
