package com.example.miniecommerce.controller;

import com.example.miniecommerce.dto.cart.request.CartRemoveProductRequestDto;
import com.example.miniecommerce.dto.cart.request.CartAddProductRequestDto;
import com.example.miniecommerce.dto.cart.response.CartResponseDto;
import com.example.miniecommerce.mapper.cart.CartMapper;
import com.example.miniecommerce.service.CartService;
import com.example.miniecommerce.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/v1/cart")
@RequiredArgsConstructor
public class CartController {
    private final CartService cartService;
    private final UserService userService;
    private final CartMapper cartMapper;

    @GetMapping
    public CartResponseDto getCart(Principal principal) {
        return cartMapper.cartToCartResponseDto(userService.getUserByUsername(principal.getName()).getCart());
    }

    @PostMapping("/add")
    public CartResponseDto addProductToCart(CartAddProductRequestDto dto, Principal principal) {
        return cartMapper.cartToCartResponseDto(
                cartService.addProduct(
                        dto.getProductId(),
                        dto.getProductQuantity(),
                        userService.getUserByUsername(principal.getName()))
        );
    }

    @PostMapping("/remove")
    public CartResponseDto removeProductFromCart(CartRemoveProductRequestDto dto, Principal principal) {
        return cartMapper.cartToCartResponseDto(
                cartService.removeProduct(
                        dto.getProductId(),
                        dto.getProductQuantity(),
                        userService.getUserByUsername(principal.getName()))
        );
    }

    @DeleteMapping
    public CartResponseDto emptyCart(Principal principal) {
        return cartMapper.cartToCartResponseDto(
                cartService.emptyCartFor(userService.getUserByUsername(principal.getName()))
        );
    }
}
