package com.example.miniecommerce.mapper.order;

import com.example.miniecommerce.dto.order.response.OrderResponseDto;
import com.example.miniecommerce.model.purchase.Order;

public interface OrderMapper {
    OrderResponseDto orderToOrderResponseDto(Order order);
}
