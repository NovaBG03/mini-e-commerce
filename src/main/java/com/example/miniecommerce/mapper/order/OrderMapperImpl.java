package com.example.miniecommerce.mapper.order;

import com.example.miniecommerce.dto.cart.response.RequestedProductResponseDto;
import com.example.miniecommerce.dto.order.response.OrderResponseDto;
import com.example.miniecommerce.mapper.product.ProductMapper;
import com.example.miniecommerce.model.purchase.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class OrderMapperImpl implements OrderMapper {
    private final ProductMapper productMapper;

    @Override
    public OrderResponseDto orderToOrderResponseDto(Order order) {
        if (order == null) {
            return null;
        }
        final var products = productMapper
                .requestedProductsToRequestedProductResponseDtos(order.getRequestedProducts());
        final var totalPrice = products.stream()
                .map(RequestedProductResponseDto::getTotalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return OrderResponseDto.builder()
                .products(products)
                .totalPrice(totalPrice)
                .isFulfilled(order.isFulfilled())
                .build();
    }
}
