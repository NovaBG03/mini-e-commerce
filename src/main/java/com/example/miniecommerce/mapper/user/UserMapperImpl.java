package com.example.miniecommerce.mapper.user;

import com.example.miniecommerce.dto.user.request.UserPatchRequestDto;
import com.example.miniecommerce.dto.user.request.UserRegisterRequestDto;
import com.example.miniecommerce.dto.user.response.UserDataResponseDto;
import com.example.miniecommerce.model.EcomUser;
import org.springframework.stereotype.Component;

@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public EcomUser userRegisterRequestDtoToUser(UserRegisterRequestDto dto) {
        if (dto == null) {
            return null;
        }
        return EcomUser.builder()
                .username(dto.getUsername())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .email(dto.getEmail())
                .phoneNumber(dto.getPhoneNumber())
                .address(dto.getAddress())
                .deliveryPreferences(dto.getDeliveryPreferences())
                .build();
    }

    @Override
    public EcomUser userPatchRequestDtoToUser(UserPatchRequestDto dto) {
        return EcomUser.builder()
                .username(dto.getUsername())
                .email(dto.getEmail())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .address(dto.getAddress())
                .phoneNumber(dto.getPhoneNumber())
                .deliveryPreferences(dto.getDeliveryPreferences())
                .build();
    }

    @Override
    public UserDataResponseDto userToUserDataResponseDto(EcomUser user) {
        if (user == null) {
            return null;
        }
        return UserDataResponseDto.builder()
                .username(user.getUsername())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .address(user.getAddress())
                .phoneNumber(user.getPhoneNumber())
                .deliveryPreferences(user.getDeliveryPreferences())
                .build();
    }
}
