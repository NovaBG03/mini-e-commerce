package com.example.miniecommerce.mapper.user;

import com.example.miniecommerce.dto.user.request.UserPatchRequestDto;
import com.example.miniecommerce.dto.user.request.UserRegisterRequestDto;
import com.example.miniecommerce.dto.user.response.UserDataResponseDto;
import com.example.miniecommerce.model.EcomUser;

public interface UserMapper {
    EcomUser userRegisterRequestDtoToUser(UserRegisterRequestDto dto);
    EcomUser userPatchRequestDtoToUser(UserPatchRequestDto dto);

    UserDataResponseDto userToUserDataResponseDto(EcomUser user);
}
