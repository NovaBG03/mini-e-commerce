package com.example.miniecommerce.mapper.product;

import com.example.miniecommerce.dto.cart.response.RequestedProductResponseDto;
import com.example.miniecommerce.dto.product.request.ProductCreateRequestDto;
import com.example.miniecommerce.dto.product.request.ProductUpdateRequestDto;
import com.example.miniecommerce.dto.product.response.ProductPageResponseDto;
import com.example.miniecommerce.dto.product.response.ProductResponseDto;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.model.product.RequestedProduct;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface ProductMapper {
    Product productCreateRequestDtoToProduct(ProductCreateRequestDto dto);
    Product productUpdateRequestDtoToProduct(ProductUpdateRequestDto dto);

    ProductResponseDto productToProductResponseDto(Product product);
    ProductPageResponseDto productPageToProductPageResponseDto(Page<Product> productPage);
    RequestedProductResponseDto requestedProductToRequestedProductResponseDto(RequestedProduct requestedProduct);
    Collection<RequestedProductResponseDto> requestedProductsToRequestedProductResponseDtos(Collection<RequestedProduct> requestedProducts);
}
