package com.example.miniecommerce.mapper.product;

import com.example.miniecommerce.dto.cart.response.RequestedProductResponseDto;
import com.example.miniecommerce.dto.product.request.ProductCreateRequestDto;
import com.example.miniecommerce.dto.product.request.ProductUpdateRequestDto;
import com.example.miniecommerce.dto.product.response.ProductPageResponseDto;
import com.example.miniecommerce.dto.product.response.ProductResponseDto;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.model.product.ProductStatus;
import com.example.miniecommerce.model.product.RequestedProduct;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class ProductMapperImpl implements ProductMapper {
    @Override
    public Product productCreateRequestDtoToProduct(ProductCreateRequestDto dto) {
        if (dto == null) {
            return null;
        }
        return Product.builder()
                .code(dto.getCode())
                .name(dto.getName())
                .description(dto.getDescription())
                .price(dto.getPrice())
                .quantity(dto.getQuantity())
                .status(ProductStatus.valueOf(dto.getStatus().toUpperCase()))
                .build();
    }

    @Override
    public Product productUpdateRequestDtoToProduct(ProductUpdateRequestDto dto) {
        if (dto == null) {
            return null;
        }
        return Product.builder()
                .id(dto.getId())
                .code(dto.getCode())
                .name(dto.getName())
                .description(dto.getDescription())
                .price(dto.getPrice())
                .quantity(dto.getQuantity())
                .status(ProductStatus.valueOf(dto.getStatus().toUpperCase()))
                .build();
    }

    @Override
    public ProductResponseDto productToProductResponseDto(Product product) {
        if (product == null) {
            return null;
        }
        return ProductResponseDto.builder()
                .id(product.getId())
                .code(product.getCode())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .status(product.getStatus().toString())
                .build();
    }

    @Override
    public ProductPageResponseDto productPageToProductPageResponseDto(Page<Product> productPage) {
        if (productPage == null) {
            return null;
        }
        return ProductPageResponseDto.builder()
                .products(productPage.get()
                        .map(this::productToProductResponseDto)
                        .collect(Collectors.toList()))
                .totalPages(productPage.getTotalPages())
                .totalElements(productPage.getTotalElements())
                .build();
    }

    @Override
    public RequestedProductResponseDto requestedProductToRequestedProductResponseDto(RequestedProduct requestedProduct) {
        if (requestedProduct == null) {
            return null;
        }
        final var selectedQuantity = requestedProduct.getSelectedQuantity();
        final var singleUnitPrice = requestedProduct.getProduct().getPrice();
        final var totalPrice = singleUnitPrice.multiply(BigDecimal.valueOf(selectedQuantity));
        return RequestedProductResponseDto.builder()
                .productId(requestedProduct.getProduct().getId())
                .selectedQuantity(selectedQuantity)
                .singleUnitPrice(singleUnitPrice)
                .totalPrice(totalPrice)
                .build();
    }

    @Override
    public Collection<RequestedProductResponseDto> requestedProductsToRequestedProductResponseDtos(Collection<RequestedProduct> requestedProducts) {
        return requestedProducts
                .stream()
                .map(this::requestedProductToRequestedProductResponseDto)
                .collect(Collectors.toList());
    }
}
