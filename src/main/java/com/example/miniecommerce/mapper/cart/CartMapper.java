package com.example.miniecommerce.mapper.cart;

import com.example.miniecommerce.dto.cart.response.CartResponseDto;
import com.example.miniecommerce.model.purchase.Cart;

public interface CartMapper {
    CartResponseDto cartToCartResponseDto(Cart cart);
}
