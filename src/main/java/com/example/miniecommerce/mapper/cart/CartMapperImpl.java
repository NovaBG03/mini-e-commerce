package com.example.miniecommerce.mapper.cart;

import com.example.miniecommerce.dto.cart.response.RequestedProductResponseDto;
import com.example.miniecommerce.dto.cart.response.CartResponseDto;
import com.example.miniecommerce.mapper.product.ProductMapper;
import com.example.miniecommerce.model.purchase.Cart;
import com.example.miniecommerce.model.product.RequestedProduct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CartMapperImpl implements CartMapper {
    private final ProductMapper productMapper;

    @Override
    public CartResponseDto cartToCartResponseDto(Cart cart) {
        if (cart == null) {
            return null;
        }
        final var products = productMapper
                .requestedProductsToRequestedProductResponseDtos(cart.getRequestedProducts());
        final var totalPrice = products.stream()
                .map(RequestedProductResponseDto::getTotalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return CartResponseDto.builder()
                .products(products)
                .totalPrice(totalPrice)
                .build();
    }
}
