package com.example.miniecommerce;

import com.example.miniecommerce.service.jwt.JwtProps;
import com.example.miniecommerce.service.validator.ValidatorProps;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({JwtProps.class, ValidatorProps.class})
public class MiniECommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniECommerceApplication.class, args);
	}

}
