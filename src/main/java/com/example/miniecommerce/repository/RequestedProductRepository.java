package com.example.miniecommerce.repository;

import com.example.miniecommerce.model.product.RequestedProduct;
import com.example.miniecommerce.model.product.RequestedProductId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestedProductRepository extends CrudRepository<RequestedProduct, RequestedProductId> {
}

