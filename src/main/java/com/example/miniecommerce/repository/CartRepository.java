package com.example.miniecommerce.repository;

import com.example.miniecommerce.model.purchase.Cart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository<Cart, Long> {
}
