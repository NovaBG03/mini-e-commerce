package com.example.miniecommerce.repository;

import com.example.miniecommerce.model.product.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    boolean existsByCodeAndIdIsNot(String code, Long id);

    Page<Product> findAll(Specification<Product> specification, Pageable pageable);
}
