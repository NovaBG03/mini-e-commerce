package com.example.miniecommerce.dto.product.response;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductPageResponseDto {
    @Builder.Default
    private List<ProductResponseDto> products = new ArrayList<>();
    private int totalPages;
    private long totalElements;
}
