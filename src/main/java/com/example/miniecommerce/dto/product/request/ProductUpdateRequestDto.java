package com.example.miniecommerce.dto.product.request;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductUpdateRequestDto {
    private Long id;
    private String code;
    private String name;
    private String description;
    private BigDecimal price;
    private int quantity;
    private String status;
}
