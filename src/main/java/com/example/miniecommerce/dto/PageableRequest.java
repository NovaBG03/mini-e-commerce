package com.example.miniecommerce.dto;

import lombok.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageableRequest {
    private int page = 0;
    private int size = 5;

    public Pageable get() {
        return PageRequest.of(page, size);
    }
}
