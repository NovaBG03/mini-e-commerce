package com.example.miniecommerce.dto.user.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAuthenticationRequestDto {
    private String username;
    private String password;
}
