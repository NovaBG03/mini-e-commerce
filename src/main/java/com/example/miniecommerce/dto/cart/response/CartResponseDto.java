package com.example.miniecommerce.dto.cart.response;

import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartResponseDto {
    @Builder.Default
    private Collection<RequestedProductResponseDto> products = new ArrayList<>();
    @Builder.Default
    private BigDecimal totalPrice = BigDecimal.ZERO;
}
