package com.example.miniecommerce.dto.cart.response;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestedProductResponseDto {
    private Long productId;
    private int selectedQuantity;
    @Builder.Default
    private BigDecimal singleUnitPrice = BigDecimal.ZERO;
    @Builder.Default
    private BigDecimal totalPrice = BigDecimal.ZERO;
}
