package com.example.miniecommerce.dto.cart.request;

import lombok.*;

import javax.validation.constraints.Min;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartRemoveProductRequestDto {
    private Long productId;

    @Min(value = 1, message = "PRODUCT_QUANTITY_INVALID")
    private int productQuantity = 1;
}
