package com.example.miniecommerce.dto.order.response;

import com.example.miniecommerce.dto.cart.response.RequestedProductResponseDto;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderResponseDto {
    @Builder.Default
    private Collection<RequestedProductResponseDto> products = new ArrayList<>();
    @Builder.Default
    private BigDecimal totalPrice = BigDecimal.ZERO;
    private boolean isFulfilled;
}
