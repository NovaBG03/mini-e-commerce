package com.example.miniecommerce.model.product;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Embeddable
public class RequestedProductId implements Serializable {
    private Long purchaseRequestId;
    private Long productId;
}
