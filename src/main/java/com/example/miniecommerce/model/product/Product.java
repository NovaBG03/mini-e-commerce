package com.example.miniecommerce.model.product;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "PRODUCT_CODE_REQUIRED")
    @Column(unique = true)
    private String code;

    @NotBlank(message = "PRODUCT_NAME_REQUIRED")
    private String name;

    private String description;

    @NotNull(message = "PRODUCT_PRICE_REQUIRED")
    @DecimalMin(value = "0.0", inclusive = false, message = "PRODUCT_PRICE_INVALID")
    @Digits(integer = 10, fraction = 2, message = "PRODUCT_PRICE_INVALID")
    private BigDecimal price;

    @Min(value = 0, message = "PRODUCT_QUANTITY_INVALID")
    private int quantity;

    @NotNull(message = "PRODUCT_STATUS_REQUIRED")
    @Enumerated(EnumType.ORDINAL)
    private ProductStatus status;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private Collection<RequestedProduct> requestedProducts = new HashSet<>();

    public void addQuantity(int quantity) {
        this.quantity += quantity;
    }

    public void removeQuantity(int quantity) {
        this.quantity -= quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Product product = (Product) o;
        return id != null && Objects.equals(id, product.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
