package com.example.miniecommerce.model.product;

public enum ProductStatus {
    DISCONTINUED, OFFLINE, ONLINE
}
