package com.example.miniecommerce.model.product;

import com.example.miniecommerce.model.purchase.Cart;
import com.example.miniecommerce.model.purchase.PurchaseRequest;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "product_request")
public class RequestedProduct {
    @EmbeddedId
    private RequestedProductId id;

    @ManyToOne
    @MapsId("purchaseRequestId")
    private PurchaseRequest purchaseRequest;

    @ManyToOne
    @MapsId("productId")
    private Product product;

    @Min(value = 1, message = "CART_PRODUCT_SELECTED_QUANTITY_INVALID")
    private int selectedQuantity;

    @Builder
    public RequestedProduct(PurchaseRequest purchaseRequest, Product product, int selectedQuantity) {
        this.id = RequestedProductId.builder()
                .purchaseRequestId(purchaseRequest.getId())
                .productId(product.getId())
                .build();
        this.purchaseRequest = purchaseRequest;
        this.product = product;
        this.selectedQuantity = selectedQuantity;
    }

    public void addQuantity(int quantity) {
        this.selectedQuantity += quantity;
    }

    public void removeQuantity(int quantity) {
        this.selectedQuantity -= quantity;
    }
}
