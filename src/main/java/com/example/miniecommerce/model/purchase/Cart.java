package com.example.miniecommerce.model.purchase;

import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.model.product.RequestedProduct;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.Collection;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Cart extends PurchaseRequest {
    @Builder
    public Cart(Long id, EcomUser user, Collection<RequestedProduct> requestedProducts) {
        super(id, user, requestedProducts);
    }

    public RequestedProduct addProduct(Product product, int productQuantity) {
        final var cartProductOptional = this.requestedProducts.stream()
                .filter(cp -> cp.getProduct().equals(product))
                .findFirst();

        RequestedProduct requestedProduct;
        if (cartProductOptional.isPresent()) {
            requestedProduct = cartProductOptional.get();
        } else {
            requestedProduct = RequestedProduct.builder().product(product).purchaseRequest(this).build();
            this.requestedProducts.add(requestedProduct);
        }

        requestedProduct.addQuantity(productQuantity);
        return requestedProduct;
    }

    public RequestedProduct removeProduct(Product product, int productQuantity) {
        final var requestedProductOptional = findRequestedProduct(product);

        if (requestedProductOptional.isEmpty()) {
            return null;
        }
        final var requestedProduct = requestedProductOptional.get();

        requestedProduct.removeQuantity(productQuantity);

        if (requestedProduct.getSelectedQuantity() <= 0) {
            this.requestedProducts.remove(requestedProduct);
            requestedProduct.setPurchaseRequest(null);
        }

        return requestedProduct;
    }

    public boolean isEmpty() {
        return requestedProducts.isEmpty();
    }
}
