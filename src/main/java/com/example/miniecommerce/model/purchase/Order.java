package com.example.miniecommerce.model.purchase;

import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.product.RequestedProduct;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "order_request")
public class Order extends PurchaseRequest {

    @Builder
    public Order(Long id, EcomUser user, Collection<RequestedProduct> requestedProducts, boolean isFulfilled) {
        super(id, user, requestedProducts);
        this.isFulfilled = isFulfilled;
    }

    private boolean isFulfilled = false;

    public static Order from(Cart cart) {
        final var order = Order.builder()
                .user(cart.getUser())
                .build();

        final var orderedProducts = cart.getRequestedProducts()
                .stream()
                .map(rp -> RequestedProduct.builder()
                        .product(rp.getProduct())
                        .selectedQuantity(rp.getSelectedQuantity())
                        .purchaseRequest(order)
                        .build())
                .collect(Collectors.toList());

        order.setRequestedProducts(orderedProducts);

        return order;
    }
}
