package com.example.miniecommerce.model.purchase;

import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.model.product.RequestedProduct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class PurchaseRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private EcomUser user;

    @OneToMany(mappedBy = "purchaseRequest", cascade = CascadeType.ALL)
    protected Collection<RequestedProduct> requestedProducts = new HashSet<>();

    public PurchaseRequest(Long id, EcomUser user, Collection<RequestedProduct> requestedProducts) {
        this.id = id;
        this.user = user;
        if (requestedProducts != null) {
            this.requestedProducts = requestedProducts;
        }
    }

    protected Optional<RequestedProduct> findRequestedProduct(Product product) {
        return requestedProducts
                .stream()
                .filter(cp -> cp.getProduct().equals(product))
                .findFirst();
    }
}
