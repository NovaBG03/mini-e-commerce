package com.example.miniecommerce.model;

import com.example.miniecommerce.model.purchase.Cart;
import com.example.miniecommerce.model.purchase.Order;
import com.example.miniecommerce.model.purchase.PurchaseRequest;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "ecom_user")
public class EcomUser {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(nullable = false, updatable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID id;

    @NotBlank(message = "USER_USERNAME_REQUIRED")
    @Length.List({
            @Length(min = 3, message = "USER_USERNAME_MIN_LENGTH_3"),
            @Length(max = 255, message = "USER_USERNAME_MAX_LENGTH_255"),
    })
    @Column(unique = true)
    private String username;

    @NotBlank(message = "USER_PASSWORD_REQUIRED")
    private String encodedPassword;

    @NotBlank(message = "USER_FIRST_NAME_REQUIRED")
    @Length.List({
            @Length(min = 1, message = "USER_FIRST_NAME_MIN_LENGTH_3"),
            @Length(max = 255, message = "USER_FIRST_NAME_MAX_LENGTH_255"),
    })
    private String firstName;

    @NotBlank(message = "USER_LAST_NAME_REQUIRED")
    @Length.List({
            @Length(min = 1, message = "USER_LAST_NAME_MIN_LENGTH_3"),
            @Length(max = 255, message = "USER_LAST_NAME_MAX_LENGTH_255"),
    })
    private String lastName;

    @NotBlank(message = "USER_EMAIL_REQUIRED")
    @Email(message = "USER_EMAIL_INVALID")
    @Column(unique = true)
    private String email;

    @NotBlank(message = "USER_PHONE_NUMBER_REQUIRED")
    @Pattern(regexp = "(^$|\\d{10})", message = "USER_PHONE_NUMBER_INVALID")
    private String phoneNumber;

    @NotBlank(message = "USER_ADDRESS_REQUIRED")
    @Length.List({
            @Length(min = 10, message = "USER_ADDRESS_MIN_LENGTH_10"),
            @Length(max = 255, message = "USER_ADDRESS_MAX_LENGTH_255"),
    })
    private String address;

    private String deliveryPreferences;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Collection<PurchaseRequest> purchaseRequests = new HashSet<>();

    @Builder
    public EcomUser(UUID id,
                    String username,
                    String encodedPassword,
                    String firstName,
                    String lastName,
                    String email,
                    String phoneNumber,
                    String address,
                    String deliveryPreferences,
                    Cart cart,
                    Collection<Order> orders) {
        this.id = id;
        this.username = username;
        this.encodedPassword = encodedPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.deliveryPreferences = deliveryPreferences;
        if (cart != null) {
            this.setCart(cart);
        }
        if (orders != null) {
            this.addOrders(orders);
        }
    }

    public void addOrder(Order order) {
        purchaseRequests.add(order);
    }

    public void addOrders(Collection<Order> orders) {
        purchaseRequests.addAll(orders);
    }

    public Collection<Order> getOrders() {
        return getRequestsFor(Order.class).collect(Collectors.toSet());
    }

    public void setCart(Cart cart) {
        this.purchaseRequests.removeIf(x -> x.getClass().equals(Cart.class));
        this.purchaseRequests.add(cart);
    }

    public Cart getCart() {
        return getRequestsFor(Cart.class)
                .findFirst()
                .orElse(null);
    }

    private <T extends PurchaseRequest> Stream<T> getRequestsFor(Class<T> purchaseRequestClass) {
        return purchaseRequests.stream()
                .filter(x -> x.getClass().equals(purchaseRequestClass))
                .map(purchaseRequestClass::cast);
    }
}
