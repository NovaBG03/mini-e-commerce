package com.example.miniecommerce.filtering.product;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductFilterCriteria {
    private String code;
    private String productName;
    private String description;
}
