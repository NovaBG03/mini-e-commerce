package com.example.miniecommerce.filtering.product;

import com.example.miniecommerce.model.product.Product;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductSpecificationFactory {
    public Specification<Product> createSpecification(ProductFilterCriteria filterCriteria) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.and(getSpecificationPredicates(filterCriteria, root, criteriaBuilder));
    }

    private Predicate[] getSpecificationPredicates(ProductFilterCriteria filterCriteria,
                                                   Root<Product> root,
                                                   CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (filterCriteria.getCode() != null) {
            predicates.add(
                    criteriaBuilder.like(root.get("code"), "%" + filterCriteria.getCode() + "%")
            );
        }

        if (filterCriteria.getProductName() != null) {
            predicates.add(
                    criteriaBuilder.like(root.get("name"), "%" + filterCriteria.getProductName() + "%")
            );
        }

        if (filterCriteria.getDescription() != null) {
            predicates.add(
                    criteriaBuilder.like(root.get("description"), "%" + filterCriteria.getDescription() + "%")
            );
        }

        return predicates.toArray(Predicate[]::new);
    }
}
