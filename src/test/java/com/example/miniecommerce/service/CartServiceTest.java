package com.example.miniecommerce.service;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.model.product.RequestedProduct;
import com.example.miniecommerce.model.purchase.Cart;
import com.example.miniecommerce.repository.CartRepository;
import com.example.miniecommerce.repository.RequestedProductRepository;
import com.example.miniecommerce.service.validator.ModelValidatorService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CartServiceTest {
    @Mock
    ProductService productService;
    @Mock
    ModelValidatorService modelValidatorService;
    @Mock
    CartRepository cartRepository;
    @Mock
    RequestedProductRepository requestedProductRepository;

    @InjectMocks
    CartService cartService;

    Long defaultProductId = 1L;
    int defaultProductQuantity = 10;

    Product defaultProduct = Product.builder()
            .id(defaultProductId)
            .quantity(defaultProductQuantity)
            .build();

    Cart defaultCart = Cart.builder().build();
    EcomUser defaultUser = EcomUser.builder()
            .cart(defaultCart)
            .build();

    @Test
    @DisplayName("Should add new product to cart successfully")
    void shouldAddNewProductToCartSuccessfully() {
        final var requestedProductQuantity = 1;
        final var newQuantityInCart = 1;

        when(productService.getById(defaultProductId)).thenReturn(defaultProduct);
        when(cartRepository.save(defaultCart)).then(AdditionalAnswers.returnsFirstArg());

        final var updatedCart = cartService.addProduct(defaultProductId, requestedProductQuantity, defaultUser);

        assertThat(updatedCart
                .getRequestedProducts()
                .stream()
                .filter(x -> x.getProduct().equals(defaultProduct))
                .map(RequestedProduct::getSelectedQuantity)
                .findFirst()
                .orElse(null))
                .isEqualTo(newQuantityInCart);

        verify(modelValidatorService, atLeastOnce()).validate(any(RequestedProduct.class));
        verify(modelValidatorService, atLeastOnce()).validate(defaultCart);
        verify(cartRepository, atLeastOnce()).save(defaultCart);
    }

    @Test
    @DisplayName("Test increase product quantity in cart successfully")
    void testIncreaseProductQuantityInCartSuccessfully() {
        final var requestedProductQuantity = 1;
        final var initialProductQuantityInCart = 3;
        final var newQuantityInCart = requestedProductQuantity + initialProductQuantityInCart;

        final var requestedProduct = RequestedProduct.builder()
                .product(defaultProduct)
                .selectedQuantity(initialProductQuantityInCart)
                .purchaseRequest(defaultCart)
                .build();

        defaultCart.getRequestedProducts().add(requestedProduct);

        when(productService.getById(defaultProductId)).thenReturn(defaultProduct);
        when(cartRepository.save(any(Cart.class))).then(AdditionalAnswers.returnsFirstArg());

        final var updatedCart = cartService.addProduct(defaultProductId, requestedProductQuantity, defaultUser);
        assertThat(updatedCart
                .getRequestedProducts()
                .stream()
                .filter(x -> x.getProduct().equals(defaultProduct))
                .map(RequestedProduct::getSelectedQuantity)
                .findFirst()
                .orElse(null))
                .isEqualTo(newQuantityInCart);

        verify(modelValidatorService, atLeastOnce()).validate(any(RequestedProduct.class));
        verify(modelValidatorService, atLeastOnce()).validate(defaultCart);
        verify(cartRepository, atLeastOnce()).save(defaultCart);
    }

    @Test
    @DisplayName("Should throw exception when add new product invalid quantity")
    void shouldThrowExceptionWhenAddNewProductInvalidQuantity() {
        final int invalidProductQuantity = -3;
        assertThatThrownBy(() -> cartService.addProduct(defaultProductId, invalidProductQuantity, defaultUser))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("PRODUCT_QUANTITY_INVALID");
    }

    @Test
    @DisplayName("Should throw exception when add new product quantity exceeds available")
    void shouldThrowExceptionWhenAddNewProductQuantityExceedsAvailable() {
        final var requestedProductQuantity = 1;
        defaultProduct.setQuantity(0);
        when(productService.getById(defaultProductId)).thenReturn(defaultProduct);

        assertThatThrownBy(() -> cartService.addProduct(defaultProductId, requestedProductQuantity, defaultUser))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("PRODUCT_QUANTITY_EXCEEDS_AVAILABLE");
    }

    @Test
    @DisplayName("Should remove product from cart successfully")
    void shouldRemoveProductFromCartSuccessfully() {
        final var requestedProductQuantity = 1;
        final var initialProductQuantityInCart = 1;

        final var requestedProduct = RequestedProduct.builder()
                .product(defaultProduct)
                .selectedQuantity(initialProductQuantityInCart)
                .purchaseRequest(defaultCart)
                .build();

        defaultCart.getRequestedProducts().add(requestedProduct);

        when(productService.getById(defaultProductId)).thenReturn(defaultProduct);
        when(cartRepository.save(any(Cart.class))).then(AdditionalAnswers.returnsFirstArg());

        final var updatedCart = cartService.removeProduct(defaultProductId, requestedProductQuantity, defaultUser);
        assertThat(updatedCart.getRequestedProducts().isEmpty()).isTrue();

        verify(requestedProductRepository, atLeastOnce()).delete(requestedProduct);
        verify(modelValidatorService, atLeastOnce()).validate(defaultCart);
        verify(cartRepository, atLeastOnce()).save(defaultCart);
    }

    @Test
    @DisplayName("Should decrease product quantity from cart successfully")
    void shouldDecreaseProductQuantityFromCartSuccessfully() {
        final var requestedProductQuantity = 1;
        final var initialProductQuantityInCart = 3;
        final var newQuantityInCart = initialProductQuantityInCart - requestedProductQuantity;

        final var requestedProduct = RequestedProduct.builder()
                .product(defaultProduct)
                .selectedQuantity(initialProductQuantityInCart)
                .purchaseRequest(defaultCart)
                .build();

        defaultCart.getRequestedProducts().add(requestedProduct);

        when(productService.getById(defaultProductId)).thenReturn(defaultProduct);
        when(cartRepository.save(any(Cart.class))).then(AdditionalAnswers.returnsFirstArg());

        final var updatedCart = cartService.removeProduct(defaultProductId, requestedProductQuantity, defaultUser);
        assertThat(updatedCart
                .getRequestedProducts()
                .stream()
                .filter(x -> x.getProduct().equals(defaultProduct))
                .map(RequestedProduct::getSelectedQuantity)
                .findFirst()
                .orElse(null))
                .isEqualTo(newQuantityInCart);

        verify(modelValidatorService, atLeastOnce()).validate(requestedProduct);
        verify(modelValidatorService, atLeastOnce()).validate(defaultCart);
        verify(cartRepository, atLeastOnce()).save(defaultCart);
    }

    @Test
    @DisplayName("Should throw exception when remove product invalid quantity")
    void shouldThrowExceptionWhenRemoveProductInvalidQuantity() {
        final int invalidProductQuantity = -3;

        assertThatThrownBy(() -> cartService.removeProduct(defaultProductId, invalidProductQuantity, defaultUser))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("PRODUCT_QUANTITY_INVALID");
    }

    @Test
    @DisplayName("Should throw exception when remove product not in cart")
    void shouldThrowExceptionWhenRemoveProductNotInCart() {
        final var requestedProductQuantity = 1;

        when(productService.getById(defaultProductId)).thenReturn(defaultProduct);

        assertThatThrownBy(() -> cartService.removeProduct(defaultProductId, requestedProductQuantity, defaultUser))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("PRODUCT_NOT_IN_CART");
    }

    @Test
    @DisplayName("Should empty cart for user successfully")
    void shouldEmptyCartForUserSuccessfully() {
        final var requestedProducts = new ArrayList<RequestedProduct>();
        requestedProducts.add(RequestedProduct.builder()
                .purchaseRequest(defaultCart)
                .product(Product.builder().id(1L).build())
                .build());
        requestedProducts.add(RequestedProduct.builder()
                .purchaseRequest(defaultCart)
                .product(Product.builder().id(1L).build())
                .build());
        requestedProducts.add(RequestedProduct.builder()
                .purchaseRequest(defaultCart)
                .product(Product.builder().id(1L).build())
                .build());
        defaultCart.setRequestedProducts(requestedProducts);

        final var updatedCart = cartService.emptyCartFor(defaultUser);
        assertThat(updatedCart.getRequestedProducts().isEmpty()).isTrue();

        verify(requestedProductRepository, atLeastOnce()).deleteAll(requestedProducts);
    }
}