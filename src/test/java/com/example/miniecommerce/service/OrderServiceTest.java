package com.example.miniecommerce.service;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.model.purchase.Cart;
import com.example.miniecommerce.model.purchase.Order;
import com.example.miniecommerce.repository.OrderRepository;
import com.example.miniecommerce.repository.ProductRepository;
import com.example.miniecommerce.service.validator.ModelValidatorService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
    @Mock
    OrderRepository orderRepository;
    @Mock
    ProductRepository productRepository;
    @Mock
    ModelValidatorService modelValidatorService;
    @Mock
    CartService cartService;

    @InjectMocks
    OrderService orderService;

    Cart defaultCart = Cart.builder().build();

    EcomUser defaultUser = EcomUser.builder()
            .cart(defaultCart)
            .build();

    @Test
    @DisplayName("Should place order successfully")
    void shouldPlaceOrderSuccessfully() {
        int productQuantity = 5;
        Product product = Product.builder()
                .quantity(productQuantity)
                .build();

        int selectedQuantity = 3;
        defaultCart.addProduct(product, selectedQuantity);

        when(orderRepository.save(any(Order.class))).then(AdditionalAnswers.returnsFirstArg());

        final var placedOrder = orderService.placeOrder(defaultUser);
        assertThat(placedOrder.getRequestedProducts().size()).isEqualTo(1);

        verify(productRepository, atLeastOnce()).saveAll(any());
        verify(cartService, atLeastOnce()).emptyCartFor(defaultUser);
        verify(modelValidatorService, atLeastOnce()).validate(any(Order.class));
        verify(orderRepository, atLeastOnce()).save(any(Order.class));
    }

    @Test
    @DisplayName("Should throw exception when place order and cart is empty")
    void shouldThrowExceptionWhenPlaceOrderAndCartIsEmpty() {
        defaultCart.setRequestedProducts(List.of());
        assertThatThrownBy(() -> orderService.placeOrder(defaultUser))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("CART_EMPTY");
    }

    @Test
    @DisplayName("Should throw exception when place order and invalid quantity in cart for product")
    void shouldThrowExceptionWhenPlaceOrderAndInvalidQuantityInCartForProduct() {
        Long productId = 1L;
        int productQuantity = 5;
        Product product = Product.builder()
                .id(productId)
                .quantity(productQuantity)
                .build();

        int selectedQuantity = productQuantity + 1;
        defaultCart.addProduct(product, selectedQuantity);

        assertThatThrownBy(() -> orderService.placeOrder(defaultUser))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("INVALID_QUANTITY_IN_CART_FOR_PRODUCT_" + product.getId());
    }
}