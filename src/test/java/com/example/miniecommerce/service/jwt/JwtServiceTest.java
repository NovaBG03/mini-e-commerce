package com.example.miniecommerce.service.jwt;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.security.EcomUserDetails;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.security.Key;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JwtServiceTest {
    @Mock
    UserDetailsService userDetailsService;
    @Mock
    JwtProps jwtProps;

    @InjectMocks
    JwtService jwtService;

    Key secretKey = Keys.hmacShaKeyFor("supersecretkeystringmustbelongandsecurelol".getBytes());

    @Test
    @DisplayName("Should get authentication from http request successfully")
    void shouldGetAuthenticationFromHttpRequestSuccessfully() {
        HttpServletRequest httpRequest = mock(HttpServletRequest.class);
        String authPrefix = "Bearer";
        String authHeader = "Authorization";
        String username = "ivan";
        EcomUser user = EcomUser.builder()
                .username(username)
                .build();

        when(jwtProps.getHttpHeader()).thenReturn(authHeader);
        when(jwtProps.getTokenPrefix()).thenReturn(authPrefix);

        when(userDetailsService.loadUserByUsername(username)).thenReturn(new EcomUserDetails(user));
        when(jwtProps.getTokenExpirationSeconds()).thenReturn(36000L);
        when(jwtProps.getSecretKey()).thenReturn(secretKey);

        String jwt = jwtService.createJwt(username);
        when(httpRequest.getHeader(authHeader)).thenReturn(authPrefix + jwt);

        Authentication authentication = jwtService.getAuthentication(httpRequest);

        assertThat(authentication)
                .isNotNull()
                .matches(x -> x.getPrincipal().equals(username))
                .matches(x -> x.getAuthorities().isEmpty());
    }

    @Test
    @DisplayName("Should get authentication from token string successfully")
    void shouldGetAuthenticationFromTokenStringSuccessfully() {
        String authPrefix = "Bearer";
        String username = "ivan";
        EcomUser user = EcomUser.builder()
                .username(username)
                .build();

        when(userDetailsService.loadUserByUsername(username)).thenReturn(new EcomUserDetails(user));
        when(jwtProps.getTokenExpirationSeconds()).thenReturn(36000L);
        when(jwtProps.getTokenPrefix()).thenReturn(authPrefix);
        when(jwtProps.getSecretKey()).thenReturn(secretKey);

        String jwt = jwtService.createJwt(username);
        Authentication authentication = jwtService.getAuthentication(authPrefix + jwt);

        assertThat(authentication)
                .isNotNull()
                .matches(x -> x.getPrincipal().equals(username))
                .matches(x -> x.getAuthorities().isEmpty());
    }

    @Test
    @DisplayName("Should not get authentication when token is null")
    void shouldNotGetAuthenticationWhenTokenIsNull() {
        Authentication authentication = jwtService.getAuthentication((String) null);
        assertThat(authentication).isNull();
    }

    @Test
    @DisplayName("Should not get authentication when auth token header is missing")
    void shouldNotGetAuthenticationWhenAuthTokenHeaderIsMissing() {
        String authPrefix = "Bearer";
        String authorizationToken = "defnottoken";

        when(jwtProps.getTokenPrefix()).thenReturn(authPrefix);
        Authentication authentication = jwtService.getAuthentication(authorizationToken);
        assertThat(authentication).isNull();
    }

    @Test
    @DisplayName("Should not get authentication when token is missing")
    void shouldNotGetAuthenticationWhenTokenIsMissing() {
        String authPrefix = "Bearer";

        when(jwtProps.getTokenPrefix()).thenReturn(authPrefix);

        Authentication authentication = jwtService.getAuthentication(authPrefix);

        assertThat(authentication).isNull();
    }

    @Test
    @DisplayName("Should throw exception when jwt has expired")
    void shouldThrowExceptionWhenJwtHasExpired() {
        String authPrefix = "Bearer";
        String username = "ivan";
        EcomUser user = EcomUser.builder()
                .username(username)
                .build();

        when(userDetailsService.loadUserByUsername(username)).thenReturn(new EcomUserDetails(user));
        when(jwtProps.getTokenExpirationSeconds()).thenReturn(0L);
        when(jwtProps.getTokenPrefix()).thenReturn(authPrefix);
        when(jwtProps.getSecretKey()).thenReturn(secretKey);

        String jwt = jwtService.createJwt(username);
        assertThatThrownBy(() -> jwtService.getAuthentication(authPrefix + jwt))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("JWT_EXPIRED");
    }

    @Test
    @DisplayName("Should throw exception when jwt invalid")
    void shouldThrowExceptionWhenJwtInvalid() {
        String authPrefix = "Bearer";
        String jwt = "invalidtoken";

        when(jwtProps.getTokenPrefix()).thenReturn(authPrefix);
        when(jwtProps.getSecretKey()).thenReturn(secretKey);

        assertThatThrownBy(() -> jwtService.getAuthentication(authPrefix + jwt))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("JWT_INVALID");
    }

    @Test
    @DisplayName("Should create jwt successfully")
    void shouldCreateJwtSuccessfully() {
        String username = "ivan";
        EcomUser user = EcomUser.builder()
                .username(username)
                .build();

        when(userDetailsService.loadUserByUsername(username)).thenReturn(new EcomUserDetails(user));
        when(jwtProps.getTokenExpirationSeconds()).thenReturn(36000L);
        when(jwtProps.getSecretKey()).thenReturn(secretKey);

        String jwt = jwtService.createJwt(username);
        assertThat(jwt)
                .isNotNull()
                .isNotEmpty();
    }

    @Test
    @DisplayName("Should set authorization response header for user successfully")
    void shouldSetAuthorizationResponseHeaderForUserSuccessfully() {
        HttpServletResponse httpResponse = mock(HttpServletResponse.class);
        String authHeader = "Authorization";
        String authPrefix = "Bearer";
        String username = "ivan";
        EcomUser user = EcomUser.builder()
                .username(username)
                .build();

        when(userDetailsService.loadUserByUsername(username)).thenReturn(new EcomUserDetails(user));
        when(jwtProps.getTokenExpirationSeconds()).thenReturn(36000L);
        when(jwtProps.getHttpHeader()).thenReturn(authHeader);
        when(jwtProps.getTokenPrefix()).thenReturn(authPrefix);
        when(jwtProps.getSecretKey()).thenReturn(secretKey);

        jwtService.setAuthorizationResponseHeaderForUser(httpResponse, username);

        verify(httpResponse).addHeader("Access-Control-Expose-Headers", authHeader);

        ArgumentCaptor<String> authTokenArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(httpResponse).setHeader(anyString(), authTokenArgumentCaptor.capture());
        String authToken = authTokenArgumentCaptor.getValue();

        assertThat(authToken)
                .isNotNull()
                .contains(authPrefix);
    }
}