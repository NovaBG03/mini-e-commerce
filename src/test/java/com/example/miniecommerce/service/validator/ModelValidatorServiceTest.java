package com.example.miniecommerce.service.validator;

import com.example.miniecommerce.exception.EcomHttpException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ModelValidatorServiceTest {
    @Mock
    Validator validator;
    @Mock
    ValidatorProps validatorProps;

    @InjectMocks
    ModelValidatorService modelValidatorService;

    @Test
    @DisplayName("Should not throw exception when no errors found")
    void shouldNotThrowExceptionWhenNoErrorsFound() {
        Object toValidate = mock(Object.class);
        when(validator.validate(toValidate)).thenReturn(Collections.emptySet());

        modelValidatorService.validate(toValidate);
    }

    @Test
    @DisplayName("Should throw exception when error found")
    void shouldThrowExceptionWhenErrorFound() {
        Object toValidate = mock(Object.class);
        String errorMessage = "error message";
        ConstraintViolation<Object> constraintViolation = mock(ConstraintViolation.class);

        when(constraintViolation.getMessage()).thenReturn(errorMessage);
        when(validator.validate(toValidate)).thenReturn(Set.of(constraintViolation));

        assertThatThrownBy(() -> modelValidatorService.validate(toValidate))
                .isInstanceOf(RuntimeException.class)
                .hasMessage(errorMessage);
    }

    @Test
    @DisplayName("Should throw exception when validate password too short")
    void shouldThrowExceptionWhenValidatePasswordTooShort() {
        int passwordMinLength = 5;
        String password = "a".repeat(passwordMinLength - 1);

        when(validatorProps.getPasswordMinLength()).thenReturn(passwordMinLength);

        assertThatThrownBy(() -> modelValidatorService.validatePassword(password))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("USER_PASSWORD_MIN_LENGTH_" + passwordMinLength);
    }

    @Test
    @DisplayName("Should throw exception when validate password too long")
    void shouldThrowExceptionWhenValidatePasswordTooLong() {
        int passwordMinLength = 5;
        int passwordMaxLength = 10;
        String password = "a".repeat(passwordMaxLength + 1);

        when(validatorProps.getPasswordMinLength()).thenReturn(passwordMinLength);
        when(validatorProps.getPasswordMaxLength()).thenReturn(passwordMaxLength);

        assertThatThrownBy(() -> modelValidatorService.validatePassword(password))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("USER_PASSWORD_MAX_LENGTH_" + passwordMaxLength);
    }

    @Test
    @DisplayName("Should validate password successfully")
    void shouldValidatePasswordSuccessfully() {
        int passwordMinLength = 5;
        int passwordMaxLength = 10;
        String password = "a".repeat(passwordMaxLength - passwordMinLength);

        when(validatorProps.getPasswordMinLength()).thenReturn(passwordMinLength);
        when(validatorProps.getPasswordMaxLength()).thenReturn(passwordMaxLength);

        modelValidatorService.validatePassword(password);
    }
}