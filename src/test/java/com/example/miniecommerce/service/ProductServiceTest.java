package com.example.miniecommerce.service;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.model.product.ProductStatus;
import com.example.miniecommerce.repository.ProductRepository;
import com.example.miniecommerce.service.validator.ModelValidatorService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {
    @Mock
    ProductRepository productRepository;
    @Mock
    ModelValidatorService modelValidatorService;

    @InjectMocks
    ProductService productService;

    Long defaultId = 1L;
    String defaultCode = "AA344VB";
    String defaultName = "Product Name";
    String defaultDescription = "Product Description";
    BigDecimal defaultPrice = new BigDecimal(10);
    int defaultQuantity = 15;
    ProductStatus defaultStatus = ProductStatus.ONLINE;

    Product defaultProduct = Product.builder()
            .id(defaultId)
            .code(defaultCode)
            .name(defaultName)
            .description(defaultDescription)
            .price(defaultPrice)
            .quantity(defaultQuantity)
            .status(defaultStatus)
            .build();

    @Test
    @DisplayName("Should create new product successfully")
    void shouldCreateNewProductSuccessfully() {
        final long newId = 12L;
        when(productRepository.existsByCodeAndIdIsNot(defaultCode, null)).thenReturn(false);
        when(productRepository.save(defaultProduct)).then(answer -> {
            final Product product = answer.getArgument(0);
            if (product != null && product.getId() == null) {
                product.setId(newId);
            }
            return product;
        });

        final Product actual = productService.createProduct(defaultProduct);
        assertThat(actual)
                .matches(x -> x.getId().equals(newId), "Has correct Id")
                .matches(x -> x.getCode().equals(defaultCode), "Has correct Code")
                .matches(x -> x.getName().equals(defaultName), "Has correct Name")
                .matches(x -> x.getDescription().equals(defaultDescription), "Has correct Description")
                .matches(x -> x.getPrice().equals(defaultPrice), "Has correct Price")
                .matches(x -> x.getQuantity() == defaultQuantity, "Has correct Quantity")
                .matches(x -> x.getStatus().equals(defaultStatus), "Has correct Status")
                .matches(x -> x.getRequestedProducts().isEmpty(), "Has empty Requested products");

        verify(modelValidatorService, atLeastOnce()).validate(defaultProduct);
    }

    @Test
    @DisplayName("Should throw exception when create new product code exists")
    void shouldThrowExceptionWhenCreateNewProductCodeExists() {
        when(productRepository.existsByCodeAndIdIsNot(defaultCode, null)).thenReturn(true);

        assertThatThrownBy(() -> productService.createProduct(defaultProduct))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("PRODUCT_CODE_EXISTS");
    }
    
    @Test
    @DisplayName("Should update product successfully")
    void shouldUpdateProductSuccessfully() {
        when(productRepository.existsByCodeAndIdIsNot(defaultCode, defaultId)).thenReturn(false);
        when(productRepository.save(defaultProduct)).thenAnswer(AdditionalAnswers.returnsFirstArg());

        final Product actual = productService.saveOrUpdateProduct(defaultProduct);
        assertThat(actual)
                .matches(x -> x.getId().equals(defaultId), "Has correct Id")
                .matches(x -> x.getCode().equals(defaultCode), "Has correct Code")
                .matches(x -> x.getName().equals(defaultName), "Has correct Name")
                .matches(x -> x.getDescription().equals(defaultDescription), "Has correct Description")
                .matches(x -> x.getPrice().equals(defaultPrice), "Has correct Price")
                .matches(x -> x.getQuantity() == defaultQuantity, "Has correct Quantity")
                .matches(x -> x.getStatus().equals(defaultStatus), "Has correct Status")
                .matches(x -> x.getRequestedProducts().isEmpty(), "Has empty Requested products");

        verify(modelValidatorService, atLeastOnce()).validate(defaultProduct);
        verify(productRepository, atLeastOnce()).save(defaultProduct);
    }

    @Test
    @DisplayName("Should get product by id successfully")
    void shouldGetProductByIdSuccessfully() {
        when(productRepository.findById(defaultId)).thenReturn(Optional.of(defaultProduct));

        final Product actual = productService.getById(defaultId);
        assertThat(actual).isEqualTo(defaultProduct);
    }

    @Test
    @DisplayName("Should throw exception when get product by id not found")
    void shouldThrowExceptionWhenGetProductByIdNotFound() {
        when(productRepository.findById(defaultId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> productService.getById(defaultId))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("PRODUCT_NOT_FOUND");
    }

    @Test
    @DisplayName("Should delete product by id successfully")
    void shouldDeleteProductByIdSuccessfully() {
        when(productRepository.existsById(defaultId)).thenReturn(true);

        productService.deleteById(defaultId);

        verify(productRepository, atLeastOnce()).deleteById(defaultId);
    }

    @Test
    @DisplayName("Should throw exception when delete product by id not found")
    void shouldThrowExceptionWhenDeleteProductByIdNotFound() {
        when(productRepository.existsById(defaultId)).thenReturn(false);

        assertThatThrownBy(() -> productService.deleteById(defaultId))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("PRODUCT_NOT_FOUND");
    }

    @Test
    @DisplayName("Should get filtered products correctly")
    void shouldGetFilteredProductsCorrectly(@Mock Specification<Product> specification) {
        PageRequest pageRequest = PageRequest.of(0, 3);

        productService.getFilteredProducts(specification, pageRequest);

        verify(productRepository, times(1)).findAll(specification, pageRequest);
    }
}