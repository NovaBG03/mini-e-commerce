package com.example.miniecommerce.service;

import com.example.miniecommerce.exception.EcomHttpException;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.repository.UserRepository;
import com.example.miniecommerce.service.validator.ModelValidatorService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    UserRepository userRepository;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    ModelValidatorService modelValidatorService;

    @InjectMocks
    UserService userService;

    UUID defaultId = UUID.randomUUID();
    String defaultUsername = "defaultUsername";
    String defaultPassword = "password@#paSS34";
    String defaultEncodedPassword = "encoded_password";
    String defaultFirstName = "firstName";
    String defaultLastName = "lastName";
    String defaultEmail = "default@gmail.com";
    String defaultPhoneNumber = "0877303344";
    String defaultAddress = "address";
    String defaultDeliveryPreferences = "delivery preferences";
    EcomUser defaultUser = EcomUser.builder()
            .id(defaultId)
            .username(defaultUsername)
            .encodedPassword(defaultEncodedPassword)
            .firstName(defaultFirstName)
            .lastName(defaultLastName)
            .email(defaultEmail)
            .phoneNumber(defaultPhoneNumber)
            .address(defaultAddress)
            .deliveryPreferences(defaultDeliveryPreferences)
            .build();

    @Test
    @DisplayName("Should load user details by username")
    void shouldLoadUserDetailsByUsername() {
        when(userRepository.findByUsername(defaultUsername)).thenReturn(Optional.of(defaultUser));

        final UserDetails actual = userService.loadUserByUsername(defaultUsername);
        assertThat(actual)
                .matches(x -> x.getUsername().equals(defaultUsername), "Has correct username")
                .matches(x -> x.getPassword().equals(defaultEncodedPassword), "Has correct encoded password");
    }

    @Test
    @DisplayName("Should throw exception when load user details by username not found")
    void shouldThrowExceptionWhenLoadUserDetailsByUsernameNotFound() {
        when(userRepository.findByUsername(defaultUsername)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.getUserByUsername(defaultUsername))
                .isInstanceOf(UsernameNotFoundException.class);
    }

    @Test
    @DisplayName("Should get user by username")
    void shouldGetUserByUsername() {
        when(userRepository.findByUsername(defaultUsername)).thenReturn(Optional.of(defaultUser));

        final EcomUser actual = userService.getUserByUsername(defaultUsername);
        assertThat(actual).isEqualTo(defaultUser);
    }

    @Test
    @DisplayName("Should throw exception when get user by username not found")
    void shouldThrowExceptionWhenGetUserByUsernameNotFound() {
        when(userRepository.findByUsername(defaultUsername)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.getUserByUsername(defaultUsername))
                .isInstanceOf(UsernameNotFoundException.class);
    }

    @Test
    @DisplayName("Should register user successfully")
    void shouldRegisterUserSuccessfully() {
        when(userRepository.existsByUsername(defaultUsername)).thenReturn(false);
        when(userRepository.existsByEmail(defaultEmail)).thenReturn(false);
        when(passwordEncoder.encode(defaultPassword)).thenReturn(defaultEncodedPassword);
        when(userRepository.save(defaultUser)).then(AdditionalAnswers.returnsFirstArg());

        final EcomUser user = userService.register(defaultUser, defaultPassword);
        assertThat(user.getCart()).isNotNull();

        verify(modelValidatorService, atLeastOnce()).validatePassword(defaultPassword);
        verify(modelValidatorService, atLeastOnce()).validate(defaultUser);
        verify(userRepository, atLeastOnce()).save(defaultUser);
    }

    @Test
    @DisplayName("Should throw exception when register user username already exists")
    void shouldThrowExceptionWhenRegisterUserUsernameAlreadyExists() {
        when(userRepository.existsByUsername(defaultUsername)).thenReturn(true);

        assertThatThrownBy(() -> userService.register(defaultUser, defaultPassword))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("USER_USERNAME_EXISTS");
    }

    @Test
    @DisplayName("Should throw exception when register user email already exists")
    void shouldThrowExceptionWhenRegisterUserEmailAlreadyExists() {
        when(userRepository.existsByUsername(defaultUsername)).thenReturn(false);
        when(userRepository.existsByEmail(defaultEmail)).thenReturn(true);

        assertThatThrownBy(() -> userService.register(defaultUser, defaultPassword))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("USER_EMAIL_EXISTS");
    }

    @Test
    @DisplayName("Should update user data successfully")
    void shouldUpdateUserDataSuccessfully() {
        final String newUsername = "newUsername";
        final String newFirstName = "newFirstName";
        final String newLastName = "newLastName";
        final String newEmail = "new@mail.com";
        final String newPhoneNumber = "0000000000";
        final String newAddress = "new address";
        final String newDeliveryPreferences = "new delivery preferences";

        EcomUser userData = EcomUser.builder()
                .username(newUsername)
                .firstName(newFirstName)
                .lastName(newLastName)
                .email(newEmail)
                .phoneNumber(newPhoneNumber)
                .address(newAddress)
                .deliveryPreferences(newDeliveryPreferences)
                .build();
        when(userRepository.existsByUsername(newUsername)).thenReturn(false);
        when(userRepository.existsByEmail(newEmail)).thenReturn(false);
        when(userRepository.save(any(EcomUser.class))).then(AdditionalAnswers.returnsFirstArg());

        final EcomUser updatedUser = userService.updateUser(userData, defaultUser);
        assertThat(updatedUser)
                .matches(x -> x.getId().equals(defaultId))
                .matches(x -> x.getUsername().equals(newUsername))
                .matches(x -> x.getFirstName().equals(newFirstName))
                .matches(x -> x.getLastName().equals(newLastName))
                .matches(x -> x.getEmail().equals(newEmail))
                .matches(x -> x.getPhoneNumber().equals(newPhoneNumber))
                .matches(x -> x.getAddress().equals(newAddress))
                .matches(x -> x.getDeliveryPreferences().equals(newDeliveryPreferences));

        verify(modelValidatorService, atLeastOnce()).validate(any(EcomUser.class));
        verify(userRepository, atLeastOnce()).save(any(EcomUser.class));
    }

    @Test
    @DisplayName("Should throw exception when update user username already exists")
    void shouldThrowExceptionWhenUpdateUserUsernameAlreadyExists() {
        final String newUsername = "newUsername";

        EcomUser userData = EcomUser.builder()
                .username(newUsername)
                .build();
        when(userRepository.existsByUsername(newUsername)).thenReturn(true);

        assertThatThrownBy(() -> userService.updateUser(userData, defaultUser))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("USER_USERNAME_EXISTS");
    }

    @Test
    @DisplayName("Should throw exception when update user email already exists")
    void shouldThrowExceptionWhenUpdateUserEmailAlreadyExists() {
        final String newEmail = "new@mail.com";

        EcomUser userData = EcomUser.builder()
                .email(newEmail)
                .build();
        when(userRepository.existsByEmail(newEmail)).thenReturn(true);

        assertThatThrownBy(() -> userService.updateUser(userData, defaultUser))
                .isInstanceOf(EcomHttpException.class)
                .hasMessage("USER_EMAIL_EXISTS");
    }
}