package com.example.miniecommerce.controller;

import com.example.miniecommerce.dto.product.request.ProductCreateRequestDto;
import com.example.miniecommerce.dto.product.request.ProductUpdateRequestDto;
import com.example.miniecommerce.dto.product.response.ProductPageResponseDto;
import com.example.miniecommerce.dto.product.response.ProductResponseDto;
import com.example.miniecommerce.filtering.product.ProductSpecificationFactory;
import com.example.miniecommerce.mapper.product.ProductMapper;
import com.example.miniecommerce.model.product.Product;
import com.example.miniecommerce.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.miniecommerce.TestUtils.json;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class ProductControllerTest {
    @MockBean
    ProductService productService;
    @MockBean
    ProductMapper productMapper;
    @MockBean
    ProductSpecificationFactory productSpecificationFactory;

    @Autowired
    WebApplicationContext context;

    MockMvc mvc;

    String defaultUsername = "user";

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @DisplayName("Should get product by id successfully")
    void shouldGetProductByIdSuccessfully() throws Exception {
        var productId = 1L;
        var product = Product.builder().id(productId).build();
        var responseDto = ProductResponseDto.builder().build();

        when(productService.getById(productId)).thenReturn(product);
        when(productMapper.productToProductResponseDto(product)).thenReturn(responseDto);

        mvc.perform(get("/api/v1/products/" + productId).with(user(defaultUsername)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("Should get all products successfully")
    void shouldGetAllProductsSuccessfully() throws Exception {
        var page = 3;
        var size = 10;
        Specification<Product> specification = mock(Specification.class);
        Page<Product> productPage = mock(Page.class);
        var responseDto = ProductPageResponseDto.builder().build();

        when(productSpecificationFactory.createSpecification(any())).thenReturn(specification);
        when(productService.getFilteredProducts(eq(specification), eq(PageRequest.of(page, size)))).thenReturn(productPage);
        when(productMapper.productPageToProductPageResponseDto(productPage)).thenReturn(responseDto);

        mvc.perform(get("/api/v1/products").with(user(defaultUsername))
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("Should create new product successfully")
    void shouldCreateNewProductSuccessfully() throws Exception {
        final var requestDto = ProductCreateRequestDto.builder().build();
        final var product = Product.builder().build();
        final var responseDto = ProductResponseDto.builder().build();

        when(productMapper.productCreateRequestDtoToProduct(eq(requestDto))).thenReturn(product);
        when(productService.createProduct(product)).then(AdditionalAnswers.returnsFirstArg());
        when(productMapper.productToProductResponseDto(product)).thenReturn(responseDto);

        mvc.perform(post("/api/v1/products").with(user(defaultUsername))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json(requestDto)))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should save or update product successfully")
    void shouldSaveOrUpdateProductSuccessfully() throws Exception {
        final var requestDto = ProductUpdateRequestDto.builder().build();
        final var product = Product.builder().build();
        final var responseDto = ProductResponseDto.builder().build();

        when(productMapper.productUpdateRequestDtoToProduct(eq(requestDto))).thenReturn(product);
        when(productService.saveOrUpdateProduct(product)).then(AdditionalAnswers.returnsFirstArg());
        when(productMapper.productToProductResponseDto(product)).thenReturn(responseDto);

        mvc.perform(put("/api/v1/products").with(user(defaultUsername))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json(requestDto)))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should delete product successfully")
    void shouldDeleteProductSuccessfully() throws Exception {
        var productId = 1L;

        mvc.perform(delete("/api/v1/products/" + productId).with(user(defaultUsername)))
                .andExpect(status().isOk());

        verify(productService, atLeastOnce()).deleteById(productId);
    }
}
