package com.example.miniecommerce.controller;

import com.example.miniecommerce.dto.cart.response.CartResponseDto;
import com.example.miniecommerce.mapper.cart.CartMapper;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.purchase.Cart;
import com.example.miniecommerce.service.CartService;
import com.example.miniecommerce.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class CartControllerTest {
    @MockBean
    CartService cartService;
    @MockBean
    UserService userService;
    @MockBean
    CartMapper cartMapper;

    @Autowired
    WebApplicationContext context;

    MockMvc mvc;

    String username = "user";
    EcomUser user = EcomUser.builder().username(username).build();

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @DisplayName("Should get cart successfully")
    void shouldGetCartSuccessfully() throws Exception {
        var cart = Cart.builder().build();
        var responseDto = CartResponseDto.builder().build();

        user.setCart(cart);

        when(userService.getUserByUsername(username)).thenReturn(user);
        when(cartMapper.cartToCartResponseDto(cart)).thenReturn(responseDto);

        mvc.perform(get("/api/v1/cart").with(user(username)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("Should add product to cart successfully")
    void shouldAddProductToCartSuccessfully() throws Exception {
        var productId = 1L;
        var productQuantity = 20;

        var cart = Cart.builder().user(user).build();
        var responseDto = CartResponseDto.builder().build();

        when(userService.getUserByUsername(username)).thenReturn(user);
        when(cartService.addProduct(productId, productQuantity, user)).thenReturn(cart);
        when(cartMapper.cartToCartResponseDto(cart)).thenReturn(responseDto);

        mvc.perform(post("/api/v1/cart/add").with(user(username))
                        .param("productId", String.valueOf(productId))
                        .param("productQuantity", String.valueOf(productQuantity)))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should remove product from cart successfully")
    void shouldRemoveProductFromCartSuccessfully() throws Exception {
        var productId = 1L;
        var productQuantity = 20;

        var cart = Cart.builder().user(user).build();
        var responseDto = CartResponseDto.builder().build();

        when(userService.getUserByUsername(username)).thenReturn(user);
        when(cartService.addProduct(productId, productQuantity, user)).thenReturn(cart);
        when(cartMapper.cartToCartResponseDto(cart)).thenReturn(responseDto);

        mvc.perform(post("/api/v1/cart/remove").with(user(username))
                        .param("productId", String.valueOf(productId))
                        .param("productQuantity", String.valueOf(productQuantity)))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should empty cart successfully")
    void shouldEmptyCartSuccessfully() throws Exception {
        var cart = Cart.builder().build();
        var responseDto = CartResponseDto.builder().build();

        when(userService.getUserByUsername(username)).thenReturn(user);
        when(cartService.emptyCartFor(user)).thenReturn(cart);
        when(cartMapper.cartToCartResponseDto(cart)).thenReturn(responseDto);

        mvc.perform(delete("/api/v1/cart").with(user(username)))
                .andExpect(status().isOk());
    }
}