package com.example.miniecommerce.controller;

import com.example.miniecommerce.dto.order.response.OrderResponseDto;
import com.example.miniecommerce.mapper.order.OrderMapper;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.model.purchase.Order;
import com.example.miniecommerce.service.OrderService;
import com.example.miniecommerce.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class OrderControllerTest {
    @MockBean
    OrderService orderService;
    @MockBean
    UserService userService;
    @MockBean
    OrderMapper orderMapper;

    @Autowired
    WebApplicationContext context;

    MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @DisplayName("Should place order successfully")
    void shouldPlaceOrderSuccessfully() throws Exception {
        String username = "user";
        var user = EcomUser.builder()
                .username(username)
                .build();
        var order = Order.builder().build();
        var responseDto = OrderResponseDto.builder().build();

        when(userService.getUserByUsername(username)).thenReturn(user);
        when(orderService.placeOrder(user)).thenReturn(order);
        when(orderMapper.orderToOrderResponseDto(order)).thenReturn(responseDto);

        mvc.perform(post("/api/v1/orders").with(user(username)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}