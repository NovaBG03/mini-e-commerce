package com.example.miniecommerce.controller;

import com.example.miniecommerce.dto.user.request.UserPatchRequestDto;
import com.example.miniecommerce.dto.user.request.UserRegisterRequestDto;
import com.example.miniecommerce.dto.user.response.UserDataResponseDto;
import com.example.miniecommerce.mapper.user.UserMapper;
import com.example.miniecommerce.model.EcomUser;
import com.example.miniecommerce.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.miniecommerce.TestUtils.json;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class UserControllerTest {
    @MockBean
    UserService userService;
    @MockBean
    UserMapper userMapper;

    @Autowired
    WebApplicationContext context;

    MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @DisplayName("Should get principal data successfully")
    void shouldGetPrincipalDataSuccessfully() throws Exception {
        String username = "user";

        when(userService.getUserByUsername(username)).thenReturn(mock(EcomUser.class));
        when(userMapper.userToUserDataResponseDto(any(EcomUser.class))).thenReturn(UserDataResponseDto.builder().build());

        mvc.perform(get("/api/v1/user").with(user(username)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @DisplayName("Should register user successfully")
    void shouldRegisterUserSuccessfully() throws Exception {
        String password = "password";
        UserRegisterRequestDto requestDto = UserRegisterRequestDto.builder().password(password).build();
        EcomUser user = mock(EcomUser.class);
        UserDataResponseDto responseDto = UserDataResponseDto.builder().build();

        when(userMapper.userRegisterRequestDtoToUser(any(UserRegisterRequestDto.class))).thenReturn(user);
        when(userService.register(user, password)).then(AdditionalAnswers.returnsFirstArg());
        when(userMapper.userToUserDataResponseDto(user)).thenReturn(responseDto);

        mvc.perform(post("/api/v1/user/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json(requestDto)))
                .andExpect(status().isOk());

        verify(userService, atLeastOnce()).register(user, password);
    }

    @Test
    @DisplayName("Should patch user data successfully")
    void shouldPatchUserDataSuccessfully() throws Exception {
        String username = "user";

        UserPatchRequestDto requestDto = UserPatchRequestDto.builder().build();
        EcomUser newUserData = mock(EcomUser.class);
        EcomUser user = mock(EcomUser.class);
        UserDataResponseDto responseDto = UserDataResponseDto.builder().build();

        when(userMapper.userPatchRequestDtoToUser(any(UserPatchRequestDto.class))).thenReturn(newUserData);
        when(userService.getUserByUsername(username)).thenReturn(user);
        when(userService.updateUser(newUserData, user)).thenReturn(user);
        when(userMapper.userToUserDataResponseDto(user)).thenReturn(responseDto);

        mvc.perform(patch("/api/v1/user").with(user(username))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json(requestDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}